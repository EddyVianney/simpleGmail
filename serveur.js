var http = require("http");
var fs = require('fs');
var mime = require('mime');
var express = require('express');
var serveStatic = require("serve-static");
var logger = require('morgan');
var bodyParser = require("body-parser");
var favicon = require("serve-favicon");
var serviceMails = require(__dirname + "/get-mails.js");

var PORT = 8080;

serviceMails.genererMails();

var app = express();
app.use(logger(":method :url"));
//configurer les routes disponibles pour le serveur
app.use(serveStatic(__dirname));


//les fonctionnalités de l'API

//recuperer la liste des dossiers
app.get("/api/dossiers", serviceMails.getDossiers);

//recupérer un mail
app.get("/api/dossiers/:idDossier",serviceMails.getDossier) ;
//recuperer un mail

app.get("/api/dossiers/:idDossier/:idMail",serviceMails.getMail);
app.use(bodyParser.json());
//envoyer un mail  
app.post("/api/envoi",serviceMails.envoiMail);

http.createServer(app).listen(PORT);
console.log("session demarré");