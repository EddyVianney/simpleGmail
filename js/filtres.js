angular.module("MesFiltres", [])
.filter("seekWord",function(){

	return function(input, recherche)
	{
		if(recherche)
		{
			return input.replace(new RegExp("("+ recherche +")","gi"),"<span class='match'>$1</span>");
		}
		else
		{
			return input ;
		}
	}
})