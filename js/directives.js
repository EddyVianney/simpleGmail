angular.module("MesDirectives", [])
.directive("mailContent",function(){

	return {

			restrict: "E",
			template: '<div class="spacer">\
                <div class="well">\
                    <h1>{{email.subject}}</h1>\
                    <p><label>De :</label> <span>{{email.from}}</span></p>\
                    <p><label>&Agrave; :</label> <span>{{email.to}}</span></p>\
                    <p><label>Date :</label> <span>{{email.date}}</span></p>\
                </div>\
                <p ng-bind-html="email.content"></p>\
            </div>',
            scope: {

            	email : "=" 

            }
	}  
})

.directive("newMail",function(){

	return {

			restrict :"E",
			template: '<div id="newMail" class="spacer">\
                <form name="formNouveauMail">\
                    <div class="input-group">\
                        <span class="input-group-addon labelspan">A</span>\
                        <input type="email" class="form-control" ng-model="nouveauMail.to" required />\
                    </div>\
                    <div class="input-group">\
                        <span class="input-group-addon labelspan">Objet</span>\
                        <input type="text" class="form-control" ng-model="nouveauMail.subject" required/>\
                    </div>\
                    <div class="spacer">\
                        <textarea ui-tinymce="optionsTinyMce"  rows="8" cols="8"  class="form-control" ng-model="nouveauMail.content" required></textarea>\
                    </div>\
                <div class="spacer text-center">\
                    <buttton  class="btn btn-warning" ng-click="initMail()">reset</buttton>\
                    <buttton  type="submit" class="btn btn-success" ng-disabled ="formNouveauMail.$invalid" ng-click="clickMail()">send</buttton>\
                    <div class="spacer text-danger" ng-show="formNouveauMail.$dirty && formNouveauMail.$invalid">Veuillez remplir correctement tous les champs </div>\
                </div>\
                </form>\
            </div>',
            scope :{

            	sendMail : "&"
            },

            controller: function($scope)
            {
            	$scope.optionsTinyMce = {
				language: "fr_FR",
				statusbar : false,
				menubar: false
				};

				$scope.initMail = function(){

		 			$scope.nouveauMail = {
		 			from: 'Rudy',
		 			date: new Date()
		 			};

		 			if(tinyMCE.activeEditor)
		 			{
		 				tinyMCE.activeEditor.setContent("");
		 			}
		 			
		 			$scope.formNouveauMail.$setPristine();
		 		};

				$scope.clickMail = function(){

					if($scope.formNouveauMail.$valid)
					{
						$scope.sendMail({nouveauMail : $scope.nouveauMail});
					}

				}

				$scope.$on("initForm",function(){

					$scope.initMail();
				});

            }
	}

});