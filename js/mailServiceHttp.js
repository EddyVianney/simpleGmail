angular.module("MailServiceHttp", [])
.factory("mailService",function($http){

		var API = "http://localhost:8080/api/";

	return {

		getDossiers: function(){ 

			var promesse = $http.get(API + "dossiers");
			var resultat = [];
			promesse.then(function(response){

				angular.extend(resultat,response.data);
			},

				function(error){

				console.log(error);
			})

			return  resultat;
		},
		getDossier: function(dossier){

			var promesse = $http.get(API + "dossiers/" + dossier);
			var resultat = {};
			promesse.then(function(response){

				angular.extend(resultat,response.data);
			},

				function(error){

				console.log(error);
			})

			return resultat;
		},

		getEmail: function(idDossier,idMail){

			var promesse = $http.get(API + "dossiers/" + idDossier + "/" + idMail);
			var resultat = {};
			promesse.then(function(response){

				angular.extend(resultat,response.data);
			},

				function(error){

				console.log(error);
			})

			return resultat;
			
		},
		envoiEmail : function(email){

			var promesse = $http.post(API + "envoi", email);
			promesse.then(function(response){},function(error){
				alert("Erreur" + error.status + "dans l'envoie de l'email" + error.data);
			});

		}

	}

});