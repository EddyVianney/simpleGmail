angular.module("Webmail", [ "ngSanitize" ,"ui.tinymce","MailServiceHttp","MesFiltres","MesDirectives"])
.controller("WebmailCtrl", function($scope, $location,$timeout, mailService) {

	$scope.dossierCourant = null;
	$scope.emailCourant = null;
	$scope.champTri = null;
	$scope.sensTri = true;
	$scope.recherche = null;
	$scope.currentView = null;


	$scope.selectDossier = function(dossier) {
		$scope.dossierCourant = mailService.getDossier(dossier);
		$scope.currentView  = 'currentFolderView';
	}

	$scope.selectEmail = function(valDossier,email) {
		$scope.emailCourant = mailService.getEmail(valDossier,email);
		$scope.currentView = 'mailContentView';
	};
	$scope.changeChevron = function(field){

		return {

				  glyphicon: $scope.champTri == field,
				 'glyphicon-chevron-up' : $scope.champTri == field && !$scope.sensTri,
				 'glyphicon-chevron-down': $scope.champTri == field && $scope.sensTri
		}
	};
	$scope.triMails = function(field){

		if($scope.champTri == field)
		{
			$scope.sensTri = ! $scope.sensTri;
		}
		else
		{
			$scope.champTri = field;
			$scope.sensTri = false;
		}
	};
	$scope.versEmail = function(dossier, email) {

		$location.path("/" + dossier.value + "/" + email.id);
	};

	$scope.sendMail = function(nouveauMail){

			mailService.envoiEmail(nouveauMail);
			currentView = null;
	};
	
	$scope.$watch(function() {
		return $location.path();
	}, function(newPath) {
		var elts = newPath.split("/");
		if (elts.length > 1) {
			var valDossier = elts[1];
			if(valDossier == "nouveauMail")
			{
				$scope.currentView = 'newMailView';
				$scope.$broadcast("initForm");
		
			}
			else
			{
				if(elts.length > 2 )
				{
					var idMail = elts[2];
					$scope.selectEmail(valDossier,idMail);
				}
				else
				{
					$scope.selectDossier(valDossier);
				}
			}

		}
		else
		{
			$scope.currentView = null;
		}   
	});

	$scope.dossiers = mailService.getDossiers();
});
